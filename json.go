package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	json_dictionary()
	json_struct()
}

func json_dictionary() {
	d := map[string]map[string]interface{}{
		"foo": {
			"bar": "Aaa",
			"bbb": 3,
			"ccc": "日本語",
		},
	}
	fmt.Printf("data = %v\n", d)
	jsonBytes, _ := json.Marshal(d)
	fmt.Printf("jsonBytes = %v\n", jsonBytes)
	fmt.Printf("jsonBytesString = %v\n", string(jsonBytes))

	var backToInterface map[string]map[string]interface{}
	err := json.Unmarshal(jsonBytes, &backToInterface)
	if err != nil {
		panic(err)
	}
	fmt.Printf("backToInterface = %v\n", backToInterface)
	fmt.Printf("%v\n", backToInterface["foo"]["bbb"].(float64))
}

type JsonDataA struct {
	Aaa int `json:"f_aaa"`
	Bbb string
}

type JsonDataB struct {
	Ccc int `json:"f_ccc"`
	Ddd JsonDataA
}

func json_struct() {
	b := JsonDataB{
		Ccc: 1,
		Ddd: JsonDataA{
			Aaa: 100,
			Bbb: "hello",
		},
	}
	bJson, err := json.Marshal(&b)
	if err != nil {
		panic(err)
	}
	fmt.Printf("struct json: %v\n", string(bJson))

	var resB JsonDataB
	err = json.Unmarshal(bJson, &resB)
	if err != nil {
		panic(err)
	}
	fmt.Printf("struct res: %v\n", resB)
}
