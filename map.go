package main

import "fmt"

func main() {
	m := map[string]int{
		"foo": 2,
	}

	fmt.Printf("%v", m)

	for k, v := range m {
		fmt.Printf("key = %v, value = %v\n", k, v)
	}

	m["bar"] = 3
	delete(m, "foo")

	d := map[string]map[string]interface{}{
		"foo": {
			"bar": "piyo",
			"Aaa": 2,
		},
	}
	fmt.Printf("nested: %v\n", d)

	v := d["foo"]["bar"]
	fmt.Printf("v: %v\n", v)
	unknown := d["foo"]["unknown"]
	fmt.Printf("unknown: %v\n", unknown)
}
