package main

import (
	"fmt"
	"strconv"
)

func main() {
	slice1 := []int{}
	fmt.Printf("slice1 = %v\n", slice1)

	slice2 := []int{1, 2, 3}
	fmt.Printf("slice2 = %v\n", slice2)

	slice3 := append(slice2, 4, 5)
	fmt.Printf("slice2 = %v\n", slice2)
	fmt.Printf("slice3 = %v\n", slice3)

	slice4 := append(slice3, slice2...)
	fmt.Printf("slice2 = %v\n", slice2)
	fmt.Printf("slice3 = %v\n", slice3)
	fmt.Printf("slice4 = %v\n", slice4)

	res, err := array_find(slice4, func(i int) bool {
		return i == 5
	})
	fmt.Printf("array_find res = %v, err = %v\n", res, err)

	groupbyRes := array_groupby([]int{1, 2, 3, 4, 5}, func(i int) string {
		return strconv.Itoa(i % 2)
	})
	fmt.Printf("groupByRes = %v\n", groupbyRes)
}

func array_find(slice []int, cond func(int) bool) (int, error) {
	for e := range slice {
		b := cond(e)
		if b {
			return e, nil
		}
	}
	return -1, fmt.Errorf("not found")
}

func array_groupby(slice []int, cond func(int) string) map[string][]int {
	var m = map[string][]int{}
	for e := range slice {
		v := cond(e)
		arr := m[v]
		if arr == nil {
			arr = []int{}
		}
		m[v] = append(arr, e)
	}
	return m
}
