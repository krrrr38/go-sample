package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "hello world"
	fmt.Printf("%s\n", str[0:1])        // h
	fmt.Printf("%s\n", str[:])          // hello world
	fmt.Printf("%s\n", str[0:])         // hello world
	fmt.Printf("%s\n", str[0:len(str)]) // hello world
	fmt.Printf("%s\n", str[1:2])        // e

	fmt.Printf("%v\n", strings.Split(str, "o"))                    // [hell  w rld]
	fmt.Printf("%v\n", strings.Replace(str, "world", "string", 1)) // hehllo string

	mapping := map[rune]rune{
		'a': 'd',
		'b': 'e',
		'c': 'f',
	}
	input := "abc"
	var output string
	for _, v := range input {
		output += string(mapping[v])
	}
	fmt.Printf("%s\n", output) // def
}
