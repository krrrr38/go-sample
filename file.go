package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	// override original file
	data := []byte("hello world2\n")
	err := ioutil.WriteFile("/tmp/go-sample-file-write1", data, 0644)
	check(err)

	// append
	write1ForAppend, err := os.OpenFile("/tmp/go-sample-file-write1", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	check(err)
	defer write1ForAppend.Close()
	fmt.Fprintln(write1ForAppend, "append new\n")

	// override original file
	file, err := os.Create("/tmp/go-sample-file-write2")
	check(err)
	defer file.Close()

	file.WriteString("first line2\n")
	file.WriteString("second line\n")
	file.Sync()

	w := bufio.NewWriter(file)
	w.WriteString("write from bufio\n")
	w.Flush()

	// check exists
	_, err = os.Stat("/tmp/go-sample-file-write2")
	if err == nil {
		fmt.Println("/tmp/go-sample-file-write2 is exists")
	}
	_, err = os.Stat("/tmp/go-sample-file-unknown")
	if err != nil {
		fmt.Println("/tmp/go-sample-file-unknown is not exists")
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
