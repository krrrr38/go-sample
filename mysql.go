package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

type MysqlTest1 struct {
	ID    int
	Name  string
	Value string
}

type MysqlTest1JoinTest2 struct {
	ID1    int
	Name1  string
	Value1 string
	ID2    int
	Name2  string
	Value2 string
}

// create database go_sample_mysql
func main() {
	db, err := sql.Open("mysql", fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?loc=Local&parseTime=true",
		"root", "", "localhost", 3306, "go_sample_mysql",
	))
	if err != nil {
		panic(err)
	}
	db.Exec("CREATE TABLE IF NOT EXISTS test1 (id int primary key, name varchar(255), value varchar(255));")
	db.Exec("CREATE TABLE IF NOT EXISTS test2 (id int primary key, name varchar(255), value varchar(255));")
	db.Exec(`INSERT INTO test1 VALUES (1, "yamada", "yamada_1"), (2, "tanaka", "tanaka_1") ON DUPLICATE KEY UPDATE name=name;`)
	db.Exec(`INSERT INTO test2 VALUES (1, "yamada", "yamada_2"), (2, "saito", "saito_2"), (3, "tanaka", "tanaka_2") ON DUPLICATE KEY UPDATE name=name;`)

	// single value
	var cnt int
	row := db.QueryRow(`SELECT COUNT(*) FROM test1`)
	err = row.Scan(&cnt)
	if err != nil && err != sql.ErrNoRows {
		panic(err)
	}
	fmt.Printf("single value: cnt = %v\n", cnt)

	// single row to struct
	row = db.QueryRow(`SELECT * FROM test1 WHERE name = ? LIMIT 1`, "yamada")
	e := MysqlTest1{}
	err = row.Scan(&e.ID, &e.Name, &e.Value)
	if err == sql.ErrNoRows {
		panic(err)
	}
	fmt.Printf("single row: e = %v\n", e)

	// multiple row to structs
	rows, err := db.Query(fmt.Sprintf(
		"SELECT * FROM test1 LIMIT %d",
		10,
	))
	if err != nil && err != sql.ErrNoRows {
		panic(err)
	}
	var test1s []MysqlTest1
	for rows.Next() {
		e := MysqlTest1{}
		rows.Scan(&e.ID, &e.Name, &e.Value)
		test1s = append(test1s, e)
	}
	rows.Close()
	fmt.Printf("multiple rows: %v\n", test1s)

	// join
	rows, err = db.Query(fmt.Sprintf(
		"SELECT * FROM test1 JOIN test2 ON test1.name = test2.name LIMIT %d",
		10,
	))
	if err != nil && err != sql.ErrNoRows {
		panic(err)
	}
	var joins []MysqlTest1JoinTest2
	for rows.Next() {
		e := MysqlTest1JoinTest2{}
		rows.Scan(&e.ID1, &e.Name1, &e.Value1, &e.ID2, &e.Name2, &e.Value2)
		joins = append(joins, e)
	}
	rows.Close()
	fmt.Printf("joins: %v\n", joins)
}
