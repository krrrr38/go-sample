package main

import (
	"fmt"

	"github.com/go-redis/redis"
	"time"
)

var client *redis.Client

func main() {
	client = redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
		DB:   0, // use default DB
	})
	defer client.Close()

	redis_simple()
	redis_hmset()
	redis_incr()
	redis_zadd()
}

func redis_simple() {
	fmt.Printf("hello %s", "redis")

	// setex -> get
	err := client.SetNX("mykey", "hello", time.Second*10).Err()
	if err != nil {
		panic(err)
	}
	res, err := client.Get("mykey").Result()
	if err != nil {
		panic(err)
	}
	fmt.Printf("get res = %v\n", res)
}

func redis_hmset() {
	// with map
	stars1 := map[string]interface{}{
		"post1": 1,
		"post2": 3,
	}
	err := client.HMSet("stars", stars1).Err()
	if err != nil {
		panic(err)
	}
	keys := []string{"post1", "post2"}
	res, err := client.HMGet("stars", keys...).Result()
	if err != nil {
		panic(err)
	}
	fmt.Printf("hmget: res = %v\n", res)
}

func redis_incr() {
	err := client.SetNX("num_value", 10, time.Second*10).Err()
	if err != nil {
		panic(err)
	}
	v, err := client.IncrBy("num_value", 2).Result()
	if err != nil {
		panic(err)
	}
	fmt.Printf("redigo_incr v = %v\n", v)
}

func redis_zadd() {
	client.ZAdd("ranking", redis.Z{
		Member: "zset3",
		Score:  3,
	}).Err()
	client.ZAdd("ranking", redis.Z{
		Member: "zset1",
		Score:  1,
	}).Err()
	client.ZAdd("ranking", redis.Z{
		Member: "zset2",
		Score:  2,
	}).Err()
	client.ZAdd("ranking", redis.Z{
		Member: "zset4",
		Score:  4,
	}).Err()

	// ranking: 0~3の内、1~2番目を取得
	res, err := client.ZRevRangeWithScores("ranking", 1, 2).Result()
	if err != nil {
		panic(err)
	}
	for k, v := range res {
		fmt.Printf("ranking: k = %v, v = %v\n", k, v)
	}
}
