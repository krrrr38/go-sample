package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	go_func_simple()
	go_func_waitgroup()
}

func go_func_simple() {
	fmt.Println("start")
	ch := make(chan int)
	defer close(ch)
	go func() {
		fmt.Println("start in go func")
		time.Sleep(time.Second)
		fmt.Println("send in go func")
		ch <- 99
	}()
	fmt.Println("after go func")
	v := <-ch // blocking
	fmt.Printf("v = %v\n", v)
}

func go_func_waitgroup() {
	arr := []int{1, 3, 2, 7, 5, 3, 4}
	var wg sync.WaitGroup
	for _, v := range arr {
		wg.Add(1)
		go func(v int) {
			fmt.Printf("start: %v\n", v)
			time.Sleep(time.Duration(v) * time.Second)
			fmt.Printf("finish: %v\n", v)
			wg.Done()
		}(v)
	}
	fmt.Printf("wait start\n")
	wg.Wait()
	fmt.Printf("wait done\n")
}
