package main

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
	"time"
)

type Post struct {
	ID    int64  `redis:"id"`
	Title string `redis:"title"`
}

func redis_func(f func(redis.Conn)) {
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	f(conn)
}

func main() {
	redigo_simple()
	redigo_hmset()
	redigo_incr()
	redigo_zadd()
	redigo_pool()
}

func redigo_simple() {
	fmt.Printf("hello %s", "redis")

	// setex -> get
	redis_func(func(conn redis.Conn) {
		err := conn.Send("SETEX", "mykey", "10", "hello")
		if err != nil {
			panic(err)
		}

		getRes, _ := redis.String(conn.Do("GET", "mykey"))
		fmt.Printf("get res = %v\n", getRes)
	})
}

func redigo_hmset() {
	// with struct
	redis_func(func(conn redis.Conn) {
		m := &Post{
			ID:    123,
			Title: "postTitle",
		}
		conn.Send("HMSET", redis.Args{}.Add("post").AddFlat(m)...)

		hmgetStrings, _ := redis.Strings(conn.Do("HGETALL", "post"))
		fmt.Printf("hmget res = %v\n", hmgetStrings)

		hmgetValues, _ := redis.Values(conn.Do("HGETALL", "post"))
		fmt.Printf("hmget res = %v\n", hmgetValues)
		var hgetAllPost Post
		if err := redis.ScanStruct(hmgetValues, &hgetAllPost); err != nil {
			panic(err)
		}
		fmt.Printf("hmget scan struct = %v\n", hgetAllPost)
	})

	// with map
	redis_func(func(conn redis.Conn) {
		stars1 := map[string]int{
			"post1": 1,
			"post2": 3,
		}
		conn.Send("HMSET", redis.Args{}.Add("stars").AddFlat(stars1))
		conn.Do("HMGET")
	})
}

func redigo_incr() {
	redis_func(func(conn redis.Conn) {
		conn.Send("SET", "num_value", 10)
		incrby, _ := redis.Int64(conn.Do("INCRBY", "num_value", 2))
		fmt.Printf("incrby res = %v\n", incrby)
	})
}

func redigo_zadd() {
	redis_func(func(conn redis.Conn) {
		conn.Send("ZADD", "zset", 3, "zset3")
		conn.Send("ZADD", "zset", 1, "zset1")
		conn.Send("ZADD", "zset", 2, "zset2")
		zrangeByRank, _ := redis.Strings(conn.Do("ZRANGEBYSCORE", "zset", 2, 3, "withscores"))
		fmt.Printf("zrangebyrank res = %v\n", zrangeByRank)
	})
}

func redigo_pool() {
	pool := &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial:        func() (redis.Conn, error) { return redis.Dial("tcp", ":6379") },
	}
	defer pool.Close()

	res, _ := pool.Get().Do("PING")
	fmt.Printf("redigo_pool ping res = %v\n", res)
}
